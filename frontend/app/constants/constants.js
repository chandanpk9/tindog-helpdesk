const BASE_URL =
  "http://ec2-16-171-6-182.eu-north-1.compute.amazonaws.com:8001";

export const API = {
  tickets: BASE_URL + "/tickets",
};

API.ticketPage = (id) => `${API.tickets}/${id}`;
