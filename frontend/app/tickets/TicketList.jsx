import Link from "next/link";
import { API } from "../constants/constants";

const getTickets = async () => {
  try {
    const res = await fetch(API.tickets, {
      next: {
        revalidate: 0,
      },
    });
    return res.json();
  } catch (error) {
    console.log(error, "errrorororororor occuredddd");
  }
};

async function TicketList() {
  const tickets = await getTickets();
  return (
    <>
      {tickets ? (
        tickets.map((ticket) => {
          return (
            <div key={ticket._id} className="card my-5">
              <Link href={`/tickets/${ticket._id}`}>
                <h3>{ticket.title}</h3>
                <p>{ticket.body.slice(0, 200)}...</p>
                <div className={`pill ${ticket.priority}`}>
                  {ticket.priority} priority
                </div>
              </Link>
            </div>
          );
        })
      ) : (
        <p className="text-center">There are no open tickets, yay!</p>
      )}
    </>
  );
}

export default TicketList;
