import React from "react";

const loading = () => {
  return (
    <main className="text-center">
      <h2 className="text-primary">Loading...</h2>
      <p>Hopefully not for to long :D</p>
    </main>
  );
};

export default loading;
