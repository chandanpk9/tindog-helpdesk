import React from "react";
import TicketList from "./TicketList";

function page() {
  return (
    <main>
      <TicketList />
    </main>
  );
}

export default page;
