import CreateForm from "./CreateForm";

const page = () => {
  return (
    <main>
      <h1>Open a New Ticket</h1>
      <CreateForm />
    </main>
  );
};

export default page;
