import { notFound } from "next/navigation";
import { API } from "../../constants/constants.js";

export async function generateStaticParams() {
  const res = await fetch(API.tickets);
  const data = await res.json();
  return data.map((ticket) => ({ slug: ticket.id }));
}

const getTicket = async (id) => {
  const res = await fetch(API.ticketPage(id));
  if (!res) {
    notFound();
  }
  return res.json();
};

async function TicketDetail({ params }) {
  const ticket = await getTicket(params.slug);
  await new Promise((resolve) => setTimeout(resolve, 2000));
  return (
    <main>
      <nav>
        <h2>Ticket Details</h2>
      </nav>
      <div className="card">
        <h3>{ticket.title}</h3>
        <small>Created by {ticket.user_email}</small>
        <p>{ticket.body}</p>
        <div className={`pill ${ticket.priority}`}>
          {ticket.priority} priority
        </div>
      </div>
    </main>
  );
}

export default TicketDetail;
